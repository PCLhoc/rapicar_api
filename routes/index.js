var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/api/post', function(req, res) {
 	var OAuth = require('oauth'),
 		async = require('async');
    

 	var twitter_data 	= [],
 		facebook_data 	= [],
 		instagram_data  = [];



 	async.parallel([
 		function (callback){
 			/////////////////////
 			//                 //   
 			// 	   TWITTER     //
 			//                 //
 			/////////////////////

			var oauth = new OAuth.OAuth(
			  'https://api.twitter.com/oauth/request_token',
			  'https://api.twitter.com/oauth/access_token',
			  'mSfDiO2hpuDpgGBGcGcXiChPm',
			  '1EB60XYt3pP3rmoIfHF0V321TCjG6LqTBHK9ZPXy0JGVwsw4uF',
			  //'seX8d3pRB5OOXC28bCBJz0O2L',
			  //'MtukC1RETNgVXMlao4js4Jof8WlTexGILGJGk7PIVmcpY5aCJ1',
			  '1.0A',
			  null,
			  'HMAC-SHA1'
			);
			    
			oauth.get(
			  'https://api.twitter.com/1.1/statuses/user_timeline.json',
			  '243707267-uoL9ELVCg7nv1mNs7aYXzWOIlFegqFwLhF9Jsk6u',
			  'VJY3BIKE5p304SWZd1Qe7EOSvZPOttXF6dIQcP1P2gswk',
			  //'290274083-N3sZ2mI6llETUbA2o2j3TrKbMaHn89SlawqPZ5vw', //test user token
			  //	'OelrL3XUbpkYdV3AbzrSXXXMDW9wsm3lusWr9R9PTfUCl', //test user secret            
			  
			  function (e, data, resp){
			    if (e){ 
			    	console.error(e);
			    	callback();        
			    }else{
			    	//console.log(data);
			    	var dataRes = JSON.parse(data)

			    	for(var i=0, count=0; i<dataRes.length; i++){

			    		var date = new Date(dataRes[i].created_at);

			    		var newEl = {
			    			message: dataRes[i].text,
			    			type: 'twitter',
			    			date: date.getTime()
			    		}; 
			    		console.log(dataRes[i].entities);

			    		if(!!dataRes[i].entities.media){
			    			for(var e=0, t=dataRes[i].entities.media.length; e<t; e++){
			    				if(dataRes[i].entities.media[e].type == 'photo'){
			    					newEl.image = dataRes[i].entities.media[e].media_url;
			    					break;
			    				}
			    			}
			    		}

			    		twitter_data.push(newEl);

			    		count++;

			    		if(count>=3){
			    			break;
			    		}

			    	}

			    	callback();
			    }   
			         
			}); 

		},


		function (callback){
 			/////////////////////
 			//                 //   
 			// 	   FACEBOOK    //
 			//                 //
 			/////////////////////

 			//HELP
 			//http://stackoverflow.com/questions/24821352/get-public-feeds-of-a-facebook-page-in-node-js
 			// de rapicar https://graph.facebook.com/v2.5/1689494174622702/feed?access_token=852271418232160|a96d8c2d3f788577c437aed00bca64d6
 			
 			// test inwork
 			// 'https://graph.facebook.com/v2.5/744591572306245/feed?access_token=1299434426737990|d7994de621c657ad8c31a272823044ca 
 			var Request = require('request');
			Request({
				url: 'https://graph.facebook.com/v2.5/1689494174622702/feed?access_token=852271418232160|a96d8c2d3f788577c437aed00bca64d6',
				method: 'GET'

			}, function(error, response, body){
				if(!error && response.statusCode>=200 && response.statusCode<400){
					var dataResp = JSON.parse(body);	

					for(var i=0, v=0, t=dataResp.data.length; i<t; i++){
						var date = new Date(dataResp.data[i].created_time);

						facebook_data.push({
							message: dataResp.data[i].message,
							date: date.getTime(),
							type: 'facebook'
						});

						v++;

						if(v>=3){
							break;
						}

					}
					
					callback();

				}else{
					callback();
				}
			});

		},


		function (callback){
 			/////////////////////
 			//                 //   
 			// 	  INSTAGRAM    //
 			//                 //
 			/////////////////////

 			//http://rapicar.inworknet.com/#access_token=2085360474.f564668.b7401c0a85e943bc8dd6b40b7064a22d
			//https://api.instagram.com/v1/users/self/media/recent/?access_token=1701917855.9a0ff5f.e8898b3bf9b34e02b2c32c95adad0c68

			var Request = require('request');
			Request({
				url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=2085360474.f564668.b7401c0a85e943bc8dd6b40b7064a22d',
				method: 'GET'

			}, function(error, response, body){
				if(!error && response.statusCode>=200 && response.statusCode<400){
					var dataResp = JSON.parse(body);	

					for(var i=0, v=0, t=dataResp.data.length; i<t; i++){

						instagram_data.push({
							message: dataResp.data[i].caption.text || "",
							date: parseInt(dataResp.data[i].created_time),
							image: dataResp.data[i].images.standard_resolution.url,
							type: 'instagram'
						});

						v++;

						if(v>=3){
							break;
						}

					}
					
					callback();

				}else{
					callback();
				}
			});

		}

  	], 

  	function (){

  		var social_array = [];

  		social_array = social_array.concat(twitter_data);
  		social_array = social_array.concat(facebook_data);
  		social_array = social_array.concat(instagram_data);

  		social_array.sort(function(a, b){
  			
  			var order = b.date > a.date ? 1 : 0;

  			return order;
  		});

  		res.json(social_array);

  	});
});

module.exports = router;


//http://rapicar.inworknet.com/#access_token=3084016410.0b618af.8440c3fea86b49e4bb6ad659ac4350b3
//http://bobmckay.com/web/simple-tutorial-for-getting-an-instagram-clientid-and-access-token/
//http://rapicar.inworknet.com/#access_token=1701917855.9a0ff5f.e8898b3bf9b34e02b2c32c95adad0c68